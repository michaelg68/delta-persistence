package com.delta.persistence;

import android.app.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class PersistenceActivity extends Activity {


    TextView readingView;
    TextView appRestartsView;
    SharedPreferences myPreferences;
    String mPref = "delta-persistance-prefs";
    int restarts = 0;

    private static final String DATA_FILE = "delta_persistence_data_file.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persistence);

        readingView = (TextView) findViewById(R.id.displayView);
        appRestartsView = (TextView) findViewById(R.id.applicationRestarts);

        myPreferences = getPreferences(Context.MODE_PRIVATE);
        //read preferences
        int default_value = 0;
        restarts = myPreferences.getInt("NUMBER_OF_RESTARTS_OF_MYNOTEPAD", default_value);

        if (restarts == 0) { //very first time start
            Toast.makeText(PersistenceActivity.this,
                    "Congratulations with the first start of your Notepad!",
                    Toast.LENGTH_LONG).show();
        }
        restarts++;

        SharedPreferences.Editor prefEditor = myPreferences.edit();
        prefEditor.putInt("NUMBER_OF_RESTARTS_OF_MYNOTEPAD", restarts);
        prefEditor.commit();


        appRestartsView.setText(String.valueOf(restarts));

    }

    //Reading and Writing

    public String getTextFile() {
        FileInputStream readFile = null;
        String fileData = null;

        try {
            readFile = openFileInput(DATA_FILE);
            int size = readFile.available();
            byte[] buffer = new byte[size];
            readFile.read(buffer);
            readFile.close();
            fileData = new String(buffer, "UTF-8");
        } catch (FileNotFoundException e) {
            Log.d("DELTA-PERSISTENCE", "Could not find file " + DATA_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("DELTA-PERSISTENCE", "Could not read file " + DATA_FILE);
            e.printStackTrace();
        } finally {
            try {
                if (readFile != null) {
                    readFile.close();
                }
            } catch (IOException e) {
                Log.d("DELTA-PERSISTENCE", "Could not close file " + DATA_FILE);
                e.printStackTrace();
            }
        }
        return fileData;
    }

    public void saveTextFile(String content) {
        FileOutputStream writeFile = null;
        try {
            writeFile = openFileOutput(DATA_FILE, Context.MODE_PRIVATE);
            writeFile.write(content.getBytes());
        } catch (FileNotFoundException e) {
            Log.d("DELTA-PERSISTENCE", "Could not find file " + DATA_FILE);
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("DELTA-PERSISTENCE", "Could not write to file " + DATA_FILE);
            e.printStackTrace();
        } finally {
            try {
                if (writeFile != null) {
                    writeFile.close();
                }
            } catch (IOException e) {
                Log.d("DELTA-PERSISTENCE", "Could not close file " + DATA_FILE);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.persistence, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveTextFile(readingView.getText().toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        readingView.setText(getTextFile());
    }
}
